var User = require('../proxy').User;
var config = require('../config');

exports.get = function (req, res) {
  var query = req.body;
  console.log('Get user data');

  User.findByQuery(query, function(err, users) {
    if(err)
      return console.log(err);

    res.json(users);
  });
};

exports.create = function (req, res) {
  var query = req.body;

  console.log('Create new user');

  User.create(query, function(err, user) {
    if(err)
      return console.log(err);

    res.json(user);
  });
};

exports.update = function (req, res) {
  var query = req.body;
  console.log('Update user');

  var findQuery = {_id: query.id};
  var updateQuery = query;

  User.update(findQuery, updateQuery, function(err, user) {
    if(err)
      return console.log(err);

    res.json(user);
  });
};
