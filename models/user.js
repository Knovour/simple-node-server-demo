var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var timeStamps = require('mongoose-timestamp');

var UserSchema = new Schema({
  name: { type: String },
  pass: { type: String },
  email: { type: String },
  avatar: { type: String }
});

UserSchema.plugin(timeStamps); // timeStamps 自動加 createdAt 跟 updatedAt
mongoose.model('User', UserSchema);
