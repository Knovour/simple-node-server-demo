var express = require('express');
var router = express.Router();

var config = require('./config');
var Controller = require('./controllers');
var User = Controller.User;

var defaultPath = '/' + config.api_version + '/' + config.api_token;

router.get('/', Controller.index);

router.post(defaultPath + '/user/get', User.get);
router.post(defaultPath + '/user/create', User.create);
router.post(defaultPath + '/user/update', User.update);

module.exports = router;
