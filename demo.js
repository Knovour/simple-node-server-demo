// 這是對 server 發出基本 request 的 demo，裡面網址可以在瀏覽器用 jQuery ajax 發送

var unirest = require('unirest'); // 發 request 的套件

var userName = 'name' + Math.floor((Math.random() * 200) + 1);
var oldPass = 'pass';
var newPass = 'pass' + Math.floor((Math.random() * 200) + 1);

console.log('建立使用者：' + userName);
unirest
  .post('http://127.0.0.1:3000/1/d3rf345321dc/user/create')
  .send({
    name: userName,
    pass: 'pass'
  })
  .end(function (response) {
    var user = response.body;
    console.log(user);

    console.log('更新使用者密碼：' + newPass);
    unirest
      .post('http://127.0.0.1:3000/1/d3rf345321dc/user/update')
      .send({
        id: user._id,
        pass: newPass
      })
      .end(function (response2) {
        console.log(response2.body);
        console.log('取得使用者資料');
        unirest
          .post('http://127.0.0.1:3000/1/d3rf345321dc/user/get')
          .send({
            name: userName,
          })
          .end(function (response3) {
            console.log(response3.body);
          });
      });
  });
// request.post('http://127.0.0.1:3000/1/d3rf345321dc/user/create', {
//     form: {name: 'name', pass: 'pass'}
//   }, function response(err, httpResponse, body) {
//     if (err)
//       return console.error('Failed:', err);

//     else {
//       var userId = JSON.parse(body)._id;
//       console.log(userId);

//       console.log('更新使用者');
//       request.post({
//           url: 'http://127.0.0.1:3000/1/d3rf345321dc/user/update',
//           form: {
//             findQuery:   {id: userId},
//             updateQuery: {pass: 'pass2'}
//           }
//         }, function response(err, httpResponse, body2) {
//           if (err)
//             return console.error('Failed:', err);

//           else {
//             console.log('取得使用者資料');
//             request.post('http://127.0.0.1:3000/1/d3rf345321dc/user/get', {
//                 form: {
//                   name: 'name',
//                   pass: 'pass2'
//                 }
//               }, function response(err, httpResponse, body3) {
//                 if (err)
//                   return console.error('Failed:', err);

//                 console.log(body3)
//             });
//           }
//       });
//     }
// });

