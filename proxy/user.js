var User = require('../models').User;

exports.findByQuery = function(query, callback) {
  User.find(query, callback);
};

exports.create = function(query, callback) {
  var user = new User();
  for(var key in query)
    user[key] = query[key];

  user.save(callback);
};

exports.update = function(findQuery, updateQuery, callback) {
  User.find(findQuery, function(err, users) {
    var user = users[0];

    for(var key in updateQuery)
      user[key] = updateQuery[key];

    user.save(callback);
  });
};
