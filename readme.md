# 基本的 Node.js + Express Server 架構

### 基本環境
* [Node.js] (不確定 0.12 版能不能跑，有問題的話裝 0.10 版)
* [MongoDB]

### 執行前的設定
1. cd 至專案資料夾
2. `npm i -d` 安裝 package
3. 把 `config.default.js` 改成 `config.js`，在裡面修改自己要的資料
4. `node app.js` 啟動，port 3000

在另一個 cmd 執行 `node demo.js` 後開 [Robomongo] 檢視資料

### 該先看的 code
* controllers
* models
* proxy
* router.js
* package.json

### 關鍵 package
* [Express.js]
* [Mongoose]
* [Jade] (前端用，可以先不管)

`node_modules` 是相關 library 的存放位置，可以不用管

[Node.js]:https://nodejs.org/
[MongoDB]:https://www.mongodb.org/
[Express.js]:http://expressjs.com/
[Mongoose]:http://mongoosejs.com/
[Jade]:http://jade-lang.com/
[Robomongo]:http://robomongo.org/
