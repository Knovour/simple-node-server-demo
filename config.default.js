var config = {
  // mongodb
  db: 'mongodb://127.0.0.1/seed',
  db_name: 'seed',

  // RESTful api
  api_version: 1,
  api_token: 'd3rf345321dc'
};

module.exports = config;
